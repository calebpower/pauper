package com.calebpower.pauper.model.actor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import com.calebpower.pauper.model.document.Document;
import com.calebpower.pauper.model.document.Document.Type;

/**
 * Represents some organization.
 * 
 * @author Caleb L. Power
 */
public class Organization {
  
  private static Map<UUID, Organization> organizations = new HashMap<>();
  
  private Map<Type, Set<UUID>> documents = new HashMap<>();
  private UUID id = null;
  private String name = null;
  private String address = null;
  private Set<UUID> personnel = new HashSet<>();
  
  /**
   * Retrieves a set of all known organizations.
   * 
   * @return a set of Organization objects
   */
  public static Set<Organization> getOrganizations() {
    Set<Organization> organizations = new HashSet<>();
    Organization.organizations.forEach((k, v) -> organizations.add(new Organization(v)));
    return organizations;
  }
  
  /**
   * Retrieves a set of all organizations whose UUIDs exist in the provided set.
   * 
   * @param ids a set of identifiers for the organizations to retrieve
   * @return a set of Organization objects
   */
  public static Set<Organization> getOrganizations(Set<UUID> ids) {
    Set<Organization> organizations = new HashSet<>();
    ids.forEach(id -> organizations.add(getOrganization(id)));
    return organizations;
  }
  
  /**
   * Retrieves a single organization by its UUID.
   * 
   * @param uuid the organization's UUID
   * @return the Organization
   */
  public static Organization getOrganization(UUID uuid) {
    return new Organization(organizations.get(uuid));
  }
  
  /**
   * Adds or replaces an organization to or in the set of all known organizations.
   * 
   * @param organization the new organization
   */
  public static void addOrReplace(Organization organization) {
    if(organizations.containsKey(organization.getID()))
      organizations.replace(organization.getID(), organization);
    else organizations.put(organization.getID(), organization);
  }
  
  /**
   * Removes an organization from the set of all known organizations.
   * 
   * @param organization the organization to remove
   * @return {@code true} iff the organization was known and subsequently removed
   */
  public static boolean remove(Organization organization) {
    if(!organizations.containsKey(organization.getID())) return false;
    organizations.remove(organization.getID());
    return true;
  }
  
  /**
   * Clears out all known organizations.
   */
  public static void clear() {
    organizations.clear();
  }
  
  /**
   * Instantiates a new Organization object.
   */
  public Organization() {
    this.id = UUID.randomUUID();
    this.name = "";
    this.address = "";
    for(Type type : Type.values())
      documents.put(type, new HashSet<>());
  }
  
  /**
   * Instantiates an Organization object from serialized data.
   * 
   * @param organization the JSON representation of the organization
   * @throws JSONException if the data was invalid
   */
  public Organization(JSONObject organization) throws JSONException {
    this.id = UUID.fromString(organization.getString("id"));
    this.name = organization.getString("name");
    this.address = organization.getString("address");
    for(Type type : Type.values())
      documents.put(type, new HashSet<>());
  }
  
  /**
   * Copy constructor.
   * 
   * @param organization the organization to be copied
   */
  public Organization(Organization organization) {
    this.id = organization.id;
    this.name = organization.name;
    this.address = organization.address;
    for(Type type : Type.values()) {
      HashSet<UUID> documents = new HashSet<>();
      organization.documents.get(type).forEach(
          d -> documents.add(d));
      this.documents.put(type, documents);
    }
  }
  
  /**
   * Retrieves the organization's unique identifier.
   * 
   * @return the organization's UUID
   */
  public UUID getID() {
    return id;
  }
  
  /**
   * Sets the organization's unique identifier.
   * 
   * @param id the organization's UUID
   */
  public void setID(UUID id) {
    this.id = id;
  }
  
  /**
   * Retrieves the name of the organization.
   * 
   * @return the organization's name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Sets the name of the organization.
   * 
   * @param name the organization's name
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Retrieves the organization's address.
   * 
   * @return the organization's address
   */
  public String getAddress() {
    return address;
  }
  
  /**
   * Sets the organization's address.
   * 
   * @param address the organization's address
   */
  public void setAddress(String address) {
    this.address = address;
  }
  
  /**
   * Retrieves the set of all people who are tied to this organization.
   * 
   * @return a set of Person objects
   */
  public Set<Person> getPeople() {
    return Person.getPeople(personnel);
  }
  
  /**
   * Adds a person to the organization.
   * 
   * @param person the person
   */
  public void addPerson(Person person) {
    if(!personnel.contains(person.getID()))
      personnel.add(person.getID());
  }
  
  /**
   * Removes a person from the organization.
   * 
   * @param person the person
   */
  public void removePerson(Person person) {
    if(personnel.contains(person.getID()))
      personnel.remove(person.getID());
  }
  
  /**
   * Retrieves a set of this organization's documents by type.
   * 
   * @param type the document type
   * @return a set of Document objects
   */
  public Set<Document> getDocuments(Type type) {
    return Document.getDocuments(type, documents.get(type));
  }
  
  /**
   * Adds a document to this organization.
   * 
   * @param type the document type
   * @param document the document
   * @return {@code true} iff the document didn't previously exist, but it was
   *         since added
   */
  public boolean addDocument(Type type, Document document) {
    if(documents.get(type).contains(document.getID())) return false;
    documents.get(type).add(document.getID());
    return true;
  }
  
  /**
   * Removes a document from this organization.
   * 
   * @param type the document type
   * @param document the document
   * @return {@code true} iff the document previously existed and was
   *         subsequently removed
   */
  public boolean removeDocument(Type type, Document document) {
    if(!documents.get(type).contains(document.getID())) return false;
    documents.get(type).remove(document.getID());
    return true;
  }
  
  /**
   * Serializes the data for storage in a JSON object retreival system.
   * 
   * @return a JSON object representing a deep copy of this object
   */
  public JSONObject serialize() {
    return new JSONObject()
        .put("id", id.toString())
        .put("name", name)
        .put("address", address);
  }
  
}
