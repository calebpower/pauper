package com.calebpower.pauper.model.actor;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents some person.
 * 
 * @author Caleb L. Power
 */
public class Person {
  
  private static Map<UUID, Person> people = new HashMap<>();
  
  private UUID id = null;
  private String name = null;
  private List<Entry<String, String>> emails = new ArrayList<>();
  private List<Entry<String, String>> phones = new ArrayList<>();
  private Set<UUID> organizations = new HashSet<>();
  
  /**
   * Retrieves a set of all known people.
   * 
   * @return a set of Person objects
   */
  public static Set<Person> getPeople() {
    Set<Person> people = new HashSet<>();
    Person.people.forEach((k, v) -> people.add(new Person(v)));
    return people;
  }
  
  /**
   * Retrieves a set of all people whose UUIDs exist in the provided set.
   * 
   * @param ids a set of identifiers for the people to retrieve
   * @return a set of Person objects
   */
  public static Set<Person> getPeople(Set<UUID> ids) {
    Set<Person> people = new HashSet<>();
    ids.forEach(id -> people.add(getPerson(id)));
    return people;
  }
  
  /**
   * Retrieves a single person by their UUID.
   * 
   * @param uuid the person's UUID
   * @return the person
   */
  public static Person getPerson(UUID uuid) {
    return new Person(people.get(uuid));
  }
  
  /**
   * Adds or replaces a person to or in the set of all known people.
   * If this person is already known, the current object is replaced.
   * 
   * @param person the new person
   */
  public static void addOrReplace(Person person) {
    if(people.containsKey(person.getID()))
      people.replace(person.getID(), person);
    else people.put(person.getID(), person);
  }
  
  /**
   * Removes a person from the set of all known people.
   * 
   * @param person the person to remove
   * @return {@code true} iff the person was known and subsequently removed
   */
  public static boolean remove(Person person) {
    if(!people.containsKey(person.getID())) return false;
    people.remove(person.getID());
    return true;
  }
  
  /**
   * Clears out all known people.
   */
  public static void clear() {
    people.clear();
  }
  
  /**
   * Instantiates a new Person object.
   */
  public Person() {
    this.id = UUID.randomUUID();
    this.name = "";
  }
  
  /**
   * Instantiates a Person object from serialized data.
   * 
   * @param person the JSON representation of the person
   * @throws JSONException if the data was invalid
   */
  public Person(JSONObject person) throws JSONException {
    this.id = UUID.fromString(person.getString("id"));
    this.name = person.getString("name");
    
    for(Object o : person.getJSONArray("emails")) {
      JSONObject email = (JSONObject)o;
      emails.add(new SimpleEntry<>(email.getString("label"), email.getString("entry")));
    }
    
    for(Object o : person.getJSONArray("phones")) {
      JSONObject phone = (JSONObject)o;
      phones.add(new SimpleEntry<>(phone.getString("label"), phone.getString("entry")));
    }
  }
  
  /**
   * Copy constructor.
   * 
   * @param person the person to be copied
   */
  public Person(Person person) {
    this.id = person.getID();
    this.name = person.name;
  }
  
  /**
   * Retrieves the person's unique identifier.
   * 
   * @return the person's UUID
   */
  public UUID getID() {
    return id;
  }
  
  /**
   * Sets the person's unique identifier.
   * 
   * @param id the person's UUID
   */
  public void setID(UUID id) {
    this.id = id;
  }
  
  /**
   * Retrieves the person's name.
   * 
   * @return the human-readable name of the person
   */
  public String getName() {
    return name;
  }
  
  /**
   * Sets the person's name.
   * 
   * @param name the human-readable name of the person
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Retrieves a list of the person's emails and associated labels.
   * Each email has a label that denotes the purpose of the email. This method
   * returns a deep copy of the list so that it can't be modified directly
   * 
   * @return a list of kv entries, where k = the label and v = the email
   */
  public List<Entry<String, String>> getEmails() {
    List<Entry<String, String>> emails = new ArrayList<>();
    this.emails.forEach(e -> emails.add(new SimpleEntry<>(e)));
    return emails;
  }
  
  /**
   * Adds an email and its respective label to the list of known emails for
   * this person.
   * 
   * @param label the email label
   * @param email the email address
   */
  public void addEmail(String label, String email) {
    emails.add(new SimpleEntry<>(label, email));
  }
  
  /**
   * Replaces an email/label entry by list index.
   * 
   * @param label the email label
   * @param email the email number
   * @param idx the index
   * @return {@code true} iff a email/label combination existed at that index
   *         and it was successfully replaced
   */
  public boolean replaceEmail(String label, String email, int idx) {
    if(idx < 0 || idx >= emails.size()) return false;
    emails.remove(idx);
    emails.add(idx, new SimpleEntry<>(label, email));
    return true;
  }
  
  /**
   * Removes exactly one instance of the provided email/label combination iff
   * it was previously associated with this user.
   * 
   * @param label the email label
   * @param email the email address
   * @return {@code true} iff the email/label combination was known and was
   *         subsequently removed
   */
  public boolean removeEmail(String label, String email) {
    for(int i = emails.size() - 1; i >= 0; i--) {
      Entry<String, String> entry = emails.get(i);
      if(entry.getKey().equals(label) && entry.getValue().equals(email)) {
        emails.remove(i);
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Removes an email/label entry by list index.
   * 
   * @param idx the index
   * @return {@code true} iff the index was in range and its element was
   *         subsequently removed
   */
  public boolean removeEmail(int idx) {
    if(idx < 0 || idx >= emails.size()) return false;
    emails.remove(idx);
    return true;
  }
  
  /**
   * Removes all known emails for this person.
   */
  public void clearEmails() {
    emails.clear();
  }
  
  /**
   * Retrieves a list of the person's phone numbers and associated labels.
   * Each phone number has a label that denotes the purpose of the said phone
   * number. This method returns a deep copy of the list so that it can't be
   * modified directly.
   * 
   * @return a list of kv entries, where k = the label and v = the phone number
   */
  public List<Entry<String, String>> getPhones() {
    List<Entry<String, String>> phones = new ArrayList<>();
    this.phones.forEach(p -> phones.add(new SimpleEntry<>(p)));
    return phones;
  }
  
  /**
   * Adds a phone number and its respective label to the list of known phone
   * numbers for this person.
   * 
   * @param label the phone label
   * @param phone the phone number
   */
  public void addPhone(String label, String phone) {
    phones.add(new SimpleEntry<>(label, phone));
  }
  
  /**
   * Replaces an phone/label entry by list index.
   * 
   * @param label the phone label
   * @param phone the phone number
   * @param idx the index
   * @return {@code true} iff a phone/label combination existed at that index
   *         and it was successfully replaced
   */
  public boolean replacePhone(String label, String phone, int idx) {
    if(idx < 0 || idx >= phones.size()) return false;
    phones.remove(idx);
    phones.add(idx, new SimpleEntry<>(label, phone));
    return true;
  }
  
  /**
   * Removes exactly one instance of the provided phone/label combination iff
   * it was previously associated with this user.
   * 
   * @param label the phone label
   * @param phone the phone number
   * @return {@code true} iff the phone/label combination was known and was
   *         subsequently removed
   */
  public boolean removePhone(String label, String phone) {
    for(int i = phones.size() - 1; i >= 0; i--) {
      Entry<String, String> entry = phones.get(i);
      if(entry.getKey().equals(label) && entry.getValue().equals(phone)) {
        phones.remove(i);
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Removes an email/label entry by list index.
   * 
   * @param idx the index
   * @return {@code true} iff the index was in range and its element was
   *         subsequently removed
   */
  public boolean removePhone(int idx) {
    if(idx < 0 || idx >= phones.size()) return false;
    emails.remove(idx);
    return true;
  }
  
  /**
   * Removes all known phone numbers for this person.
   */
  public void clearPhones() {
    phones.clear();
  }
  
  /**
   * Retrieves the set of organizations for which this person is a contact.
   * 
   * @return a set of Organization objects
   */
  public Set<Organization> getOrganizations() {
    return Organization.getOrganizations(organizations);
  }
  
  /**
   * Adds the organization to the set of organizations for which this person is
   * a contact.
   * 
   * @param organization the organization
   */
  public void addOrganization(Organization organization) {
    if(!organizations.contains(organization.getID()))
      organizations.add(organization.getID());
  }
  
  /**
   * Removes the organization from the set or organizations for which this
   * person is a contact.
   * 
   * @param organization the organization
   */
  public void removeOrganization(Organization organization) {
    if(organizations.contains(organization.getID()))
      organizations.remove(organization.getID());
  }
  
  /**
   * Serializes the data for storage in a JSON object retrieval system.
   * 
   * @return a JSON object representing a deep copy of this object
   */
  public JSONObject serialize() {
    JSONArray emails = new JSONArray();
    this.emails.forEach(e -> emails.put(new JSONObject()
        .put("label", e.getKey())
        .put("entry", e.getValue())));
    
    JSONArray phones = new JSONArray();
    this.phones.forEach(p -> phones.put(new JSONObject()
        .put("label", p.getKey())
        .put("entry", p.getValue())));
    
    return new JSONObject()
        .put("id", id.toString())
        .put("name", name)
        .put("emails", emails)
        .put("phones", phones);
  }
  
}
