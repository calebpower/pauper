package com.calebpower.pauper.model.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.calebpower.pauper.model.actor.Organization;

/**
 * Represents some document, which will either be a bill or an invoice.
 * 
 * @author Caleb L. Power
 */
public class Document {
  
  /**
   * Denotes the document type.
   * 
   * @author Caleb L. Power
   */
  public static enum Type {
    /**
     * Denotes that this document is a bill.
     */
    BILL,
    
    /**
     * Denotes that this document is an invoice.
     */
    INVOICE
  }
  
  private static Map<Type, Map<UUID, Document>> documents = new HashMap<>();
  
  private UUID id = null;
  private UUID recipient = null;
  private List<LineItem> lineItems = new ArrayList<>();
  private BigDecimal taxAmount = null;
  private Type type = null;
  
  /**
   * Retrieves a set of all known documents with a particular document type.
   * 
   * @param type the document type
   * @return a set of Document objects
   */
  public static Set<Document> getDocuments(Type type) {
    Set<Document> documents = new HashSet<>();
    if(type != null)
      Document.documents.get(type).forEach(
          (k, v) -> documents.add(new Document(v)));
    return documents;
  }
  
  /**
   * Retrieves a set of all documents of a type whose UUIDs exist in the provided
   * set.
   * 
   * @param type the document type
   * @param ids a set of identifiers for the documents to retrieve
   * @return a set of Document objects
   */
  public static Set<Document> getDocuments(Type type, Set<UUID> ids) {
    Set<Document> documents = new HashSet<>();
    ids.forEach(i -> documents.add(getDocument(type, i)));
    return documents;
  }
  
  /**
   * Retrieves a single document by its type and UUID.
   * 
   * @param type the document's type
   * @param id the document's UUID
   * @return the document
   */
  public static Document getDocument(Type type, UUID id) {
    if(type != null)
      return new Document(documents.get(type).get(id));
    return null;
  }
  
  /**
   * Adds or replaces a document to or in the set of all known documents of a
   * particular type. If this person is already known, the current object is
   * replaced.
   * 
   * @param type the document type
   * @param document the new document
   */
  public static void addOrReplace(Type type, Document document) {
    if(documents.get(type).containsKey(document.getID()))
      documents.get(type).replace(document.getID(), document);
    else documents.get(type).put(document.getID(), document);
  }
  
  /**
   * Removes a document from the set of all known documents of a type.
   * 
   * @param type the document type
   * @param document the document to remove
   * @return {@code true} iff the document was known and subsequently removed
   */
  public static boolean remove(Type type, Document document) {
    if(!documents.get(type).containsKey(document.getID())) return false;
    documents.get(type).remove(document.getID());
    return true;
  }
  
  /**
   * Clears out all known documents.
   */
  public static void clear() {
    documents.forEach((k, v) -> v.clear());
  }
  
  static {
    for(Type type : Type.values())
      documents.put(type, new HashMap<>());
  }
  
  /**
   * Instantiates a new Document object.
   *
   * @param type the type 
   */
  public Document(Type type) {
    this.id = UUID.randomUUID();
    this.lineItems = new ArrayList<>();
    this.taxAmount = BigDecimal.ZERO.setScale(2);
    this.type = type;
  }
  
  /**
   * Instantiates a Document object from serialized data.
   * 
   * @param document the JSON representation of the person
   * @throws JSONException if the data was invalid
   */
  public Document(JSONObject document) throws JSONException {
    this.id = UUID.fromString(document.getString("id"));
    this.lineItems = new ArrayList<>();
    document.getJSONArray("lineItems").forEach(
        o -> lineItems.add(new LineItem((JSONObject)o)));
    this.taxAmount = new BigDecimal(document.getString("tax"));
    this.type = Type.valueOf(document.getString("type"));
    this.recipient = UUID.fromString(document.getString("recipient"));
  }
  
  /**
   * Copy constructor.
   * 
   * @param document the document
   */
  public Document(Document document) {
    this.id = document.getID();
    this.lineItems = new ArrayList<>();
    document.lineItems.forEach(
        i -> this.lineItems.add(i));
    this.taxAmount = document.taxAmount;
    this.type = document.type;
    this.recipient = document.recipient;
  }
  
  /**
   * Retrieves the document's unique identifier.
   * 
   * @return the document's UUID
   */
  public UUID getID() {
    return id;
  }
  
  /**
   * Sets the document's unique identifier.
   * 
   * @param id the document's UUID
   */
  public void setID(UUID id) {
    this.id = id;
  }
  
  /**
   * Retrieves the document type.
   * 
   * @return the document type
   */
  public Type getType() {
    return type;
  }
  
  /**
   * Sets the document type.
   * 
   * @param type the type
   */
  public void setType(Type type) {
    this.type = type;
  }
  
  /**
   * Retrieves the recipient organization.
   * 
   * @return the recipient organization
   */
  public Organization getRecipient() {
    return Organization.getOrganization(recipient);
  }
  
  /**
   * Sets the recipient organization.
   * 
   * @param organization the recipient organization
   */
  public void setRecipient(Organization organization) {
    this.recipient = organization.getID();
  }
  
  /**
   * Retrieves a deep copy of the line items in this document.
   * 
   * @return a list of line items
   */
  public List<LineItem> getLineItems() {
    List<LineItem> lineItems = new ArrayList<>();
    this.lineItems.forEach(i -> lineItems.add(new LineItem(i)));
    return lineItems;
  }
  
  /**
   * Adds a new line item to this document.
   * 
   * @param lineItem a list of line items
   */
  public void addLineItem(LineItem lineItem) {
    lineItems.add(lineItem);
  }
  
  /**
   * Replaces a line item by list index.
   * 
   * @param lineItem the line item
   * @param idx the index
   * @return {@code true} iff a line item existed at the index and it was
   *         subsequently replaced
   */
  public boolean replaceLineItem(LineItem lineItem, int idx) {
    if(idx < 0 || idx >= lineItems.size()) return false;
    lineItems.remove(idx);
    lineItems.add(idx, lineItem);
    return true;
  }
  
  /**
   * Removes a line item from this document.
   * 
   * @param idx the index
   * @return {@code true} iff the index was in range and its element was
   *         subsequently removed
   */
  public boolean removeLineItem(int idx) {
    if(idx < 0 || idx >= lineItems.size()) return false;
    lineItems.remove(idx);
    return true;
  }
  
  /**
   * Retrieves the amount of tax to be applied to this document.
   * 
   * @return the tax amount
   */
  public BigDecimal getTaxAmount() {
    return taxAmount;
  }
  
  /**
   * Sets the amount of tax to be applied to this document.
   * 
   * @param taxAmount the tax amount
   */
  public void setTaxAmount(BigDecimal taxAmount) {
    this.taxAmount = taxAmount;
  }
  
  /**
   * Returns the total of all line items in this list.
   * 
   * @return the net total
   */
  public BigDecimal getNetTotal() {
    BigDecimal total = BigDecimal.ZERO.setScale(2);
    for(LineItem i : lineItems)
      total = total.add(i.getTotal());
    return total;
  }
  
  /**
   * Returns the total of all line items in this list, plus taxes.
   * 
   * @return the grand total
   */
  public BigDecimal getGrandTotal() {
    return getNetTotal().add(taxAmount);
  }
  
  /**
   * Serializes the data for storage in a JSON object retrieval system.
   * 
   * @return a JSON object representing a deep copy of this object
   */
  public JSONObject serialize() {
    JSONArray lineItems = new JSONArray();
    this.lineItems.forEach(i -> lineItems.put(i.serialize()));
    return new JSONObject()
        .put("id", id.toString())
        .put("items", lineItems)
        .put("tax", taxAmount.toPlainString())
        .put("type", type.name())
        .put("recipient", recipient.toString());
  }
  
}
