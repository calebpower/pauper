package com.calebpower.pauper.model.document;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents some service product.
 * 
 * @author Caleb L. Power
 */
public class ServiceProduct {
  
  private static Map<UUID, ServiceProduct> serviceProducts = new HashMap<>();
  
  private UUID id = null;
  private String description = null;
  private BigDecimal cost = null;
  
  /**
   * Retrieves a set of all known service products.
   * 
   * @return a set of ServiceProduct objects
   */
  public static Set<ServiceProduct> getServiceProducts() {
    Set<ServiceProduct> serviceProducts = new HashSet<>();
    ServiceProduct.serviceProducts.forEach(
        (k, v) -> serviceProducts.add(new ServiceProduct(v)));
    return serviceProducts;
  }
  
  /**
   * Retrieves a single service product by its UUID.
   * 
   * @param uuid the service product's UUID
   * @return the service product
   */
  public static ServiceProduct getServiceProduct(UUID uuid) {
    return new ServiceProduct(serviceProducts.get(uuid));
  }
  
  /**
   * Adds or replaces a service product to or in the set of all known service
   * products. If this service product is already known, the current object is
   * replaced.
   * 
   * @param serviceProduct the new service product
   */
  public static void addOrReplace(ServiceProduct serviceProduct) {
    if(serviceProducts.containsKey(serviceProduct.getID()))
      serviceProducts.replace(serviceProduct.getID(), serviceProduct);
    else serviceProducts.put(serviceProduct.getID(), serviceProduct);
  }
  
  /**
   * Removes a service product from the set of all known service products.
   * 
   * @param serviceProduct the service product to remove
   * @return {@code true} iff the service product was known and subsequently
   *         removed
   */
  public static boolean remove(ServiceProduct serviceProduct) {
    if(!serviceProducts.containsKey(serviceProduct.getID())) return false;
    serviceProducts.remove(serviceProduct.getID());
    return true;
  }
  
  /**
   * Clears out all known service products.
   */
  public static void clear() {
    serviceProducts.clear();
  }
  
  /**
   * Instantiates a new ServiceProduct object.
   */
  public ServiceProduct() {
    this.id = UUID.randomUUID();
    this.description = "";
    this.cost = BigDecimal.ZERO.setScale(2);
  }
  
  /**
   * Instantiates a ServiceProduct object from serialized data.
   * 
   * @param serviceProduct the JSON representation of the service product
   * @throws JSONException if the data was invalid
   */
  public ServiceProduct(JSONObject serviceProduct) throws JSONException {
    this.id = UUID.fromString(serviceProduct.getString("id"));
    this.description = serviceProduct.getString("description");
    this.cost = serviceProduct.getBigDecimal("cost");
  }
  
  /**
   * Copy constructor.
   * 
   * @param serviceProduct the original service product.
   */
  public ServiceProduct(ServiceProduct serviceProduct) {
    this.id = serviceProduct.id;
    this.description = serviceProduct.description;
    this.cost = serviceProduct.cost;
  }
  
  /**
   * Retrieves the unique identifier of the service product.
   * 
   * @return the service product's UUID
   */
  public UUID getID() {
    return id;
  }
  
  /**
   * Sets the unique identifier of the service product.
   * 
   * @param id the service product's UUID
   */
  public void setID(UUID id) {
    this.id = id;
  }
  
  /**
   * Retrieves the description of the service product.
   * 
   * @return the service product's description
   */
  public String getDescription() {
    return description;
  }
  
  /**
   * Sets the description of the service product.
   * 
   * @param description the service product's description
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /**
   * Retrieves the generic cost of this service product.
   * 
   * @return the generic cost associated with this service product
   */
  public BigDecimal getCost() {
    return cost;
  }
  
  /**
   * Sets the generic cost of this service product.
   * 
   * @param cost the generic cost associated with this service product
   */
  public void setCost(BigDecimal cost) {
    this.cost = cost;
  }
  
  /**
   * Serializes the data for storage in a JSON object retrieval system.
   * 
   * @return a JSON object representing a deep copy of this object
   */
  public JSONObject serialize() {
    return new JSONObject()
        .put("id", id.toString())
        .put("description", description)
        .put("cost", cost.toPlainString());
  }
  
}
