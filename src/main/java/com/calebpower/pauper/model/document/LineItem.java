package com.calebpower.pauper.model.document;

import java.math.BigDecimal;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents a line item on a bill or invoice.
 * 
 * @author Caleb L. Power
 */
public class LineItem {
  
  private UUID id = null;
  private UUID serviceProduct = null;
  private BigDecimal unitCost = null;
  private int amount = 0;
  
  /**
   * Instantiates a new LineItem object.
   */
  public LineItem() {
    this.id = UUID.randomUUID();
    this.serviceProduct = null;
    this.unitCost = BigDecimal.ZERO.setScale(2);
    this.amount = 0;
  }
  
  /**
   * Instantiates a LineItem object from serialized data.
   * 
   * @param lineItem a line item in a bill or invoice
   * @throws JSONException if the data was invalid
   */
  public LineItem(JSONObject lineItem) throws JSONException {
    this.id = UUID.fromString(lineItem.getString("id"));
    this.unitCost = new BigDecimal(lineItem.getString("cost"));
    this.amount = lineItem.getInt("amount");
    
  }
  
  /**
   * Copy constructor.
   * 
   * @param lineItem the original line item
   */
  public LineItem(LineItem lineItem) {
    this.id = lineItem.id;
    this.serviceProduct = lineItem.serviceProduct;
    this.unitCost = new BigDecimal(lineItem.unitCost.toString());
    this.amount = lineItem.amount;
  }
  
  /**
   * Retrieves the unique identifier of the line item.
   * 
   * @return the line item's UUID
   */
  public UUID getID() {
    return id;
  }
  
  /**
   * Sets the unique identifier of the line item.
   * 
   * @param id the line item's UUID
   */
  public void setID(UUID id) {
    this.id = id;
  }
  
  /**
   * Retrieves a copy of the service product associated with this line item.
   * 
   * @return a service product identical to that in this line item
   */
  public ServiceProduct getServiceProduct() {
    return ServiceProduct.getServiceProduct(serviceProduct);
  }
  
  /**
   * Sets the service product to be associated with this line item.
   * 
   * @param serviceProduct the service product
   */
  public void setServiceProduct(ServiceProduct serviceProduct) {
    this.serviceProduct = serviceProduct.getID();
  }
  
  /**
   * Retrieves the custom cost associated with a single unit in this line item.
   * 
   * @return the cost of a single unit in this line item
   */
  public BigDecimal getUnitCost() {
    return unitCost;
  }
  
  /**
   * Sets the custom cost associated with a single unit in this line item.
   * 
   * @param unitCcost the cost of a single unit in this line item
   */
  public void setUnitCost(BigDecimal unitCcost) {
    this.unitCost = unitCcost;
  }
  
  /**
   * Retrieves the number of units associated with this line item.
   * 
   * @return the unit count
   */
  public int getAmount() {
    return amount;
  }
  
  /**
   * Sets the number of units associated with this line item.
   * 
   * @param amount the unit count
   */
  public void setAmount(int amount) {
    this.amount = amount;
  }
  
  /**
   * Retrieves the total cost of this line item.
   * 
   * @return the total cost
   */
  public BigDecimal getTotal() {
    return unitCost.multiply(new BigDecimal(amount).setScale(0));
  }
  
  /**
   * Serializes the data for storage in a JSON object retrieval system.
   * 
   * @return a JSON object representing a deep copy of this object
   */
  public JSONObject serialize() {
    return new JSONObject()
        .put("id", id.toString())
        .put("serviceProduct", serviceProduct.toString())
        .put("cost", unitCost.toPlainString())
        .put("amount", amount);
  }
  
}
