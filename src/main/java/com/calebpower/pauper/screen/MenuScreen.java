package com.calebpower.pauper.screen;

public class MenuScreen implements Window {

  @Override public String getTitle() {
    return "Main Menu";
  }

  @Override public String generateDisplay() {
    return "Please choose an category.\n"
        + "[1] Documents\n"
        + "[2] Organizations\n"
        + "[3] Services\n";
  }

  @Override public String getPrompt() {
    return "(1-3) > ";
  }

  @Override public Window processInput(String input) {
    System.out.println("Got " + input);
    return null;
  }

  @Override public boolean getExitStatus() {
    return false;
  }

}
