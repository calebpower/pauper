package com.calebpower.pauper.screen;

import java.io.PrintStream;

/**
 * Describes a window to be displayed in a screen.
 * 
 * @author Caleb L. Power
 */
public interface Window {
  
  /**
   * Retrieves the title of this window.
   * 
   * @return the window title 
   */
  public String getTitle();
  
  /**
   * Generates a display to be shown on the screen.
   * 
   * @return the body of the display
   */
  public String generateDisplay();
  
  /**
   * Generates a prompt to be shown on the screen.
   * 
   * @return the prompt
   */
  public String getPrompt(); 
  
  /**
   * Processes user input. Returns the next window to display if appropriate.
   * 
   * @param input the user input
   * @return a new screen, or {@code null} if user input was invalid
   */
  public Window processInput(String input);
  
  /**
   * Determines whether or not the screen should exit, based on input processed
   * up to this point.
   * 
   * @return {@code true} iff the screen should exit.
   */
  public boolean getExitStatus();
  
}
