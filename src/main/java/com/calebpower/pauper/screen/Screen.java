package com.calebpower.pauper.screen;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Describes the screen that will be displayed to the user.
 * 
 * @author Caleb L. Power
 */
public class Screen {
  
  private static final int TITLE_LINE_LENGTH = 40;
  
  private Map<String, String> register = new HashMap<>();
  private Window window = null;
  
  public void launch() {
    try(Scanner keyboard = new Scanner(System.in)) {
      boolean keepGoing = true;
      
      do {
        
        StringBuilder titleBuilder = new StringBuilder();
        for(int i = 0; i < TITLE_LINE_LENGTH - 4 - window.getTitle().length(); i++)
          titleBuilder.append('=');
        titleBuilder.append("[ ").append(window.getTitle()).append(" ]");
        while(titleBuilder.length() < 40) titleBuilder.append('=');
        
        System.out.println(window.generateDisplay());
        
        Window next = null;
        do {
          System.out.print(window.getPrompt());
          next = window.processInput(keyboard.nextLine());
        } while(next == null);
        
        keepGoing = window.getExitStatus();
        window = next;
      } while(keepGoing);
    }
  }
  
}
