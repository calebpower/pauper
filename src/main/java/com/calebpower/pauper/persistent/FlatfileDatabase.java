package com.calebpower.pauper.persistent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.calebpower.pauper.model.actor.Organization;
import com.calebpower.pauper.model.actor.Person;
import com.calebpower.pauper.model.document.Document;
import com.calebpower.pauper.model.document.ServiceProduct;

/**
 * Flatfile database driver.
 * 
 * @author Caleb L. Power
 */
public class FlatfileDatabase implements Database {
  
  private static final int DB_VERSION = 1;
  
  private File file = null;
  
  /**
   * Instantiates the flatfile database.
   * 
   * @param path the path to the database file
   */
  public FlatfileDatabase(String path) {
    this.file = new File(path);
  }
  
  /**
   * {@inheritDoc}
   */
  @Override public boolean save() {
    if(file.canRead()) {
      try(PrintWriter writer = new PrintWriter(file)) {
        JSONArray documentArr = new JSONArray();
        JSONArray serviceProductArr = new JSONArray();
        JSONArray personArr = new JSONArray();
        JSONArray organizationArr = new JSONArray();
        
        for(Document.Type type : Document.Type.values())
          Document.getDocuments(type).forEach(
              d -> documentArr.put(d.serialize()));
        
        ServiceProduct.getServiceProducts().forEach(
            sp -> serviceProductArr.put(sp.serialize()));
        
        Person.getPeople().forEach(
            p -> personArr.put(p.serialize()));
        
        Organization.getOrganizations().forEach(
            o -> organizationArr.put(o.serialize()));
        
        writer.println(new JSONObject()
            .put("version", DB_VERSION)
            .put("documents", documentArr)
            .put("serviceProducts", serviceProductArr)
            .put("people", personArr)
            .put("organizations", organizationArr));
        
        return true;
      } catch(IOException e) { }
    }
    
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override public boolean load() throws JSONException {
    if(file.canRead()) {
      try(BufferedReader reader = new BufferedReader(
          new InputStreamReader(
              new FileInputStream(file),
              StandardCharsets.UTF_8))) {
        StringBuilder stringBuilder = new StringBuilder();
        for(String line; (line = reader.readLine()) != null;)
          stringBuilder.append(line.trim());
        
        JSONObject data = new JSONObject(stringBuilder.toString());
        JSONArray organizationArr = data.getJSONArray("organizations");
        JSONArray personArr = data.getJSONArray("people");
        JSONArray serviceProductArr = data.getJSONArray("serviceProducts");
        JSONArray documentArr = data.getJSONArray("documents");
        
        Organization.clear();
        Person.clear();
        ServiceProduct.clear();
        Document.clear();
        
        organizationArr.forEach(
            o -> Organization.addOrReplace(new Organization((JSONObject)o)));
        
        personArr.forEach(
            o -> Person.addOrReplace(new Person((JSONObject)o)));
        
        serviceProductArr.forEach(
            o -> ServiceProduct.addOrReplace(new ServiceProduct((JSONObject)o)));
        
        documentArr.forEach(o -> {
          Document document = new Document((JSONObject)o);
          Document.addOrReplace(document.getType(), document);
        });
        
        return true;
      } catch(IOException e) { }
    }
    
    return false;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override public boolean reset() {
    if(file.canRead()) file.delete();
    
    try {
      file.createNewFile();
      
      try(PrintWriter writer = new PrintWriter(file)) {
        file.createNewFile();
        writer.println(new JSONObject()
            .put("version", DB_VERSION)
            .put("documents", new JSONArray())
            .put("serviceProducts", new JSONArray())
            .put("people", new JSONArray())
            .put("organizations", new JSONArray()));
        
        return true;
      }
    } catch(IOException e) { }
    
    return false;
  }
  
}
