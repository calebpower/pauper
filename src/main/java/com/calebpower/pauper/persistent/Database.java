package com.calebpower.pauper.persistent;

/**
 * Database interface for easy saving and loading of the state of things.
 * 
 * @author Caleb L. Power
 */
public interface Database {
  
  /**
   * Saves the state.
   * 
   * @return {@code true} iff saving the state was successful 
   */
  public boolean save();
  
  /**
   * Loads the state.
   * 
   * @return {@code true} iff loading the state was successful 
   */
  public boolean load();
  
  /**
   * Resets the database.
   * 
   * @return {@code true} iff resetting the state was successful 
   */
  public boolean reset();

}
