package com.calebpower.pauper;

import com.calebpower.pauper.screen.Screen;

public class Pauper {
  
  public static void main(String[] args) {
    System.out.println("Hello, world!");
    
    Screen screen = new Screen();
    screen.launch();
  }
  
}
